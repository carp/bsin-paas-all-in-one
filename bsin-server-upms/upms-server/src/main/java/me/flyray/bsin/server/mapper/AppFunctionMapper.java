package me.flyray.bsin.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.SysAppFunction;

/**
* @author bolei
* @description 针对表【sys_app_function】的数据库操作Mapper
* @createDate 2023-11-07 14:22:44
* @Entity generator.domain.SysAppFunction
*/

@Repository
@Mapper
public interface AppFunctionMapper {

    void insert(SysAppFunction appFunction);

    void deleteById(String appFunctionId);

    List<SysAppFunction> selectListByAppId(String appId);

    List<SysAppFunction> selectBaseFunctionListByAppId(String appId);

    void updateById(SysAppFunction appFunction);

}




