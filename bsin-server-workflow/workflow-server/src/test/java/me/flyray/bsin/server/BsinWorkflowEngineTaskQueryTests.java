//package me.flyray.bsin.server;
//
//import org.flowable.engine.TaskService;
//import org.flowable.task.api.Task;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.List;
//
///**
// * @author ：bolei
// * @date ：Created in 2020/9/15 9:38
// * @description：流程任务处理
// * @modified By：
// */
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class BsinWorkflowEngineTaskQueryTests {
//
//    @Autowired
//    private TaskService taskService;
//
//    /**
//     * 候选用户任务查询
//     */
//    @Test
//    public void taskCandidateUserQuery(){
//        List<Task> tasks = taskService.createTaskQuery().taskCandidateUser("kermit").list();
//
//    }
//
//    /**
//     * 候选用户组任务查询
//     */
//    @Test
//    public void taskCandidateGroupQuery(){
//        List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("kermit").list();
//    }
//
//    /**
//     * 个人代办任务查询
//     */
//    @Test
//    public void taskAssignee(){
//        List<Task> tasks = taskService.createTaskQuery().taskAssignee("c").list();
//        for (Task t:tasks) {
//            System.out.println(t.getAssignee());
//        }
//    }
//
//    /**
//     * 个人代办任务分页查询
//     */
//    @Test
//    public void taskAssigneeListPage(){
//        List<Task> tasks = taskService.createTaskQuery().taskAssignee("fozzie").listPage(1,10);
//    }
//
//
//    /**
//     * 委托任务查询
//     */
//    @Test
//    public void taskOwner(){
//        List<Task> tasks = taskService.createTaskQuery().taskOwner("fozzie").list();
//    }
//
//}
