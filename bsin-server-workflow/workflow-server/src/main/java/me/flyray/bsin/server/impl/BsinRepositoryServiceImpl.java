package me.flyray.bsin.server.impl;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.facade.service.BsinRepositoryService;
import me.flyray.bsin.server.utils.RespBodyHandler;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.FlowElement;
import org.flowable.bpmn.model.FlowNode;
import org.flowable.bpmn.model.Process;
import org.flowable.common.engine.impl.de.odysseus.el.tree.Node;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.form.api.FormDefinition;
import org.flowable.form.api.FormDeployment;
import org.flowable.form.api.FormRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class BsinRepositoryServiceImpl implements BsinRepositoryService {

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private FormRepositoryService formRepositoryService;


    /**
     * 部署流程定义
     * @param requestMap
     * @return
     */
    @Override
    public Map<String,Object> importProcessDefinition(Map<String, Object> requestMap) {
        try {
            // 1、部署流程
            Deployment deployment = repositoryService.createDeployment()
                    // .key()
                    // .name(name)
                    // .category(category)
                    // .tenantId()
                    // 通过压缩包的形式一次行多个发布
                    // .addZipInputStream()
                    // 通过InputStream的形式发布
//                    .addInputStream(file.getOriginalFilename(), file.getInputStream())
                    // 通过存放在classpath目录下的文件进行发布
                     .addClasspathResource("请假流程模型.bpmn20.xml")
                    // 通过xml字符串的形式
                    // .addString()
                    .deploy();
            ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).singleResult();
            if (processDefinition == null) {
                final ResponseCode processDefinitionDeployFail = ResponseCode.PROCESS_DEFINITION_DEPLOY_FAIL;
                throw new BusinessException(processDefinitionDeployFail);
            }

        } catch (Exception e) {
            throw new RuntimeException("导入流程定义失败：" + e.getMessage());
        }
        System.out.println("流程定义部署成功");
        return RespBodyHandler.RespBodyDto();
    }

}
