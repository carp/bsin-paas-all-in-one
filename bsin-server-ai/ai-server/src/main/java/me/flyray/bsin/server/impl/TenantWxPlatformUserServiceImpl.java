package me.flyray.bsin.server.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import me.flyray.bsin.facade.service.TenantWxPlatformUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import me.flyray.bsin.server.domain.TenantWxPlatformUser;
import me.flyray.bsin.server.mapper.AiTenantWxPlatformUserMapper;
import me.flyray.bsin.utils.BsinPageUtil;
import me.flyray.bsin.utils.Pagination;
import me.flyray.bsin.utils.RespBodyHandler;

/**
* @author bolei
* @description 针对表【ai_tenant_wxmp_user】的数据库操作Service实现
* @createDate 2023-04-28 14:02:38
*/
@Service
public class TenantWxPlatformUserServiceImpl implements TenantWxPlatformUserService {

    @Autowired
    private AiTenantWxPlatformUserMapper aiTenantWxPlatformUserMapper;

    @Override
    public Map<String, Object> delete(Map<String, Object> requestMap) {
        String serialNo = (String)requestMap.get("serialNo");
        // 删除
        aiTenantWxPlatformUserMapper.deleteById(serialNo);
        return RespBodyHandler.RespBodyDto();
    }

    @Override
    public Map<String, Object> getPageList(Map<String, Object> requestMap) {
        String name = (String)requestMap.get("name");
        String tenantId = (String)requestMap.get("tenantId");
        String phone = (String)requestMap.get("phone");
        Pagination pagination = (Pagination)requestMap.get("pagination");
        BsinPageUtil.pageNotNull(pagination);
        PageHelper.startPage(pagination.getPageNum(),pagination.getPageSize());
        List<TenantWxPlatformUser> tenantWxPlatformUsers = aiTenantWxPlatformUserMapper.selectPageList(tenantId,name,phone);
        PageInfo<TenantWxPlatformUser> pageInfo = new PageInfo<TenantWxPlatformUser>(tenantWxPlatformUsers);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);
    }

}
