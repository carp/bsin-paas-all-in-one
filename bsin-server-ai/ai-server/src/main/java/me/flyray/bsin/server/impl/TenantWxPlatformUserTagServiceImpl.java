package me.flyray.bsin.server.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import me.flyray.bsin.context.BsinServiceContext;
import me.flyray.bsin.facade.service.TenantWxPlatformUserTagService;
import me.flyray.bsin.server.domain.TenantWxPlatformUserTag;
import me.flyray.bsin.server.mapper.AiTenantWxPlatformUserTagMapper;
import me.flyray.bsin.utils.RespBodyHandler;

/**
* @author bolei
* @description 针对表【ai_tenant_wxmp_user_tag】的数据库操作Service实现
* @createDate 2023-04-28 12:46:58
*/
@Service
public class TenantWxPlatformUserTagServiceImpl implements TenantWxPlatformUserTagService {

    @Autowired
    private AiTenantWxPlatformUserTagMapper tenantWxmpUserTagMapper;

    @Override
    public Map<String, Object> add(Map<String, Object> requestMap) {
        TenantWxPlatformUserTag tenantWxmpUserTag = BsinServiceContext.getReqBodyDto(TenantWxPlatformUserTag.class, requestMap);
        tenantWxmpUserTagMapper.insert(tenantWxmpUserTag);
        return RespBodyHandler.setRespBodyDto(tenantWxmpUserTag);
    }

    @Override
    public Map<String, Object> detail(Map<String, Object> requestMap) {
        String openId = (String)requestMap.get("openId");
        List<TenantWxPlatformUserTag> tenantWxmpUserTag = tenantWxmpUserTagMapper.selectByOpenId(openId);
        return RespBodyHandler.setRespBodyListDto(tenantWxmpUserTag);
    }

}
